package com.example.mahammadmammadov.weather_accenture;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mahammadmammadov.weather_accenture.Model.Database_Bean;
import com.example.mahammadmammadov.weather_accenture.Model.Wheather_pojo;
import com.example.mahammadmammadov.weather_accenture.Database.Weather_database;
import com.example.mahammadmammadov.weather_accenture.Presenter.APIClient;
import com.example.mahammadmammadov.weather_accenture.Presenter.APIInterface;
import com.example.mahammadmammadov.weather_accenture.Presenter.GPSTracker;
import com.example.mahammadmammadov.weather_accenture.Presenter.MyServices;
import com.example.mahammadmammadov.weather_accenture.Presenter.Utils;
import com.example.mahammadmammadov.weather_accenture.View.WeatherView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements WeatherView {

    private TextView main;
    private TextView description;
    private TextView temp;
    private TextView humidity;
    private DrawerLayout drawer_layout;

    private TextView min;
    private TextView max;
    private TextView speed;
    private TextView name;
    private TextView country;
    private TextView sunrise;
    private TextView sunset;
    private ImageView image1;
    static String base_url = "http://api.openweathermap.org/data/2.5/";
    String city_name = "baku";
    String appid = "fe420196a0f2ec837bfdb580365c1f9c";

//
//    public static javax.crypto.SecretKey SecretKey = null;
//    public static IvParameterSpec Iv = null;
//
//    public static byte[] Ciphertext = null;
//    public static String saltGlobal = null;


    Double a;

    Weather_database db;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {


            drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
            db = new Weather_database(getApplicationContext());

            configViews();
            get_weather();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getWeather_api() {

        GPSTracker tracker = new GPSTracker(this);
        MyServices services = new MyServices(this);
        services.getWeather_data(tracker.getLatitude(), tracker.getLongitude(), "metric", appid, base_url);


    }


    private void switch_icon(String icon) {
        switch (icon) {
            case "01d":
                image1.setImageResource(R.drawable.sunny);
                break;
            case "02d":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "03d":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "04d":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "04n":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "10d":
                image1.setImageResource(R.drawable.rain);
                break;
            case "11d":
                image1.setImageResource(R.drawable.storm);
                break;
            case "13d":
                image1.setImageResource(R.drawable.snowflake);
                break;
            case "01n":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "02n":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "03n":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "10n":
                image1.setImageResource(R.drawable.cloud);
                break;
            case "11n":
                image1.setImageResource(R.drawable.rain);
                break;
            case "13n":
                image1.setImageResource(R.drawable.snowflake);
                break;
        }
    }


    private void NotConnected() {
        Snackbar snackbar = Snackbar
                .make(drawer_layout, "Not connected to the api.", Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        snackbar.show();

    }


    private String GetUnit(String value) {
        Log.i("Unit---", value);
        String val = "°C";

        if ("AZ".equals(value)) {
            val = "°C";
        }

        if (("US".equals(value)) || ("LR".equals(value)) || ("MM".equals(val))) {
            val = "°F";
        }
        return val;
    }

    private void get_weather() {

        if (getNetworkAvailability()) {
            getWeather_api();
        } else {
            Use_localdata();
        }
    }


    private void Use_localdata() {

        List<Database_Bean> dataList = db.getAllData();
        Snackbar snackbar = Snackbar.make(drawer_layout, "loading ..... local stored data", Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        snackbar.show();

        for (Database_Bean p : dataList) {

            main.setText(String.valueOf(p.getMain()));
            description.setText(String.valueOf(p.getDescription()));
            temp.setText(String.valueOf(p.getTemp()) + GetUnit(String.valueOf(getApplication().getResources().getConfiguration().locale.getCountry())));
            humidity.setText(String.valueOf(p.getHumidity()) + " per cent");
            min.setText(String.valueOf(p.getTemp_min()) + " min");
            max.setText(String.valueOf(p.getTemp_max()) + " max");
            speed.setText(String.valueOf(p.getSpeed()) + " miles/hour");
            name.setText(String.valueOf(p.getName()));
            country.setText(String.valueOf(p.getCountry()));
            sunrise.setText(UnixTime(Long.parseLong(String.valueOf(p.getSunrise()))));
            sunset.setText(UnixTime(Long.parseLong(String.valueOf(p.getSunset()))));
            switch_icon(p.getIcon());

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.sync) {
            get_weather();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean getNetworkAvailability() {
        return Utils.isNetworkAvailable(getApplicationContext());
    }

    private String UnixTime(long timex) {

        Date date = new Date(timex * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);

    }

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void configViews() {
        main = (TextView) findViewById(R.id.main);
        description = (TextView) findViewById(R.id.description);
        temp = (TextView) findViewById(R.id.temp);
        humidity = (TextView) findViewById(R.id.humidity);
        min = (TextView) findViewById(R.id.min);
        max = (TextView) findViewById(R.id.max);
        speed = (TextView) findViewById(R.id.speed);
        name = (TextView) findViewById(R.id.name);
        country = (TextView) findViewById(R.id.country);
        sunrise = (TextView) findViewById(R.id.sunrise);
        sunset = (TextView) findViewById(R.id.sunset);

        image1 = (ImageView) findViewById(R.id.image1);
        ImageView image2 = (ImageView) findViewById(R.id.image2);
        ImageView image3 = (ImageView) findViewById(R.id.image3);
        ImageView image4 = (ImageView) findViewById(R.id.image4);
        ImageView image5 = (ImageView) findViewById(R.id.image5);
        ImageView image6 = (ImageView) findViewById(R.id.image6);
        ImageView image7 = (ImageView) findViewById(R.id.image7);

        image2.setImageResource(R.drawable.humidity);
        image3.setImageResource(R.drawable.temperature);
        image4.setImageResource(R.drawable.wind);
        image5.setImageResource(R.drawable.location);
        image6.setImageResource(R.drawable.sunrise);
        image7.setImageResource(R.drawable.sunset);
    }


    @Override
    public void getWeather(Wheather_pojo p) {
        if (p != null) {

            for (int i = 0; i < p.getWeather().size(); i++) {

                Log.i("NAM--", String.valueOf(p.getWeather().get(i).getMain()));

                if (db.TableNotEmpty()) {

                    db.updateValues("1", p.getWeather().get(i).getMain(),
                            p.getWeather().get(i).getDescription(),
                            p.getWeather().get(i).getIcon(), p.getMain().getTemp().toString(),
                            p.getMain().getHumidity().toString(), p.getMain().getTemp_min().toString(),
                            p.getMain().getTemp_max().toString(), p.getWind().getSpeed().toString(),
                            p.getSys().getCountry(), p.getSys().getSunrise().toString(),
                            p.getSys().getSunset().toString(), p.getName());

                } else {

                    db.InsertData(p.getWeather().get(i).getMain(),
                            p.getWeather().get(i).getDescription(),
                            p.getWeather().get(i).getIcon(), p.getMain().getTemp().toString(),
                            p.getMain().getHumidity().toString(), p.getMain().getTemp_min().toString(),
                            p.getMain().getTemp_max().toString(), p.getWind().getSpeed().toString(),
                            p.getSys().getCountry(), p.getSys().getSunrise().toString(),
                            p.getSys().getSunset().toString(), p.getName());

                }

                main.setText(String.valueOf(p.getWeather().get(i).getMain()));
                description.setText(String.valueOf(p.getWeather().get(i).getDescription()));
                temp.setText(String.valueOf(p.getMain().getTemp()) + GetUnit(String.valueOf(getApplication().getResources().getConfiguration().locale.getCountry())));
                humidity.setText(String.valueOf(p.getMain().getHumidity()) + " PER CENT");
                min.setText(String.valueOf(p.getMain().getTemp_min()) + " min");
                max.setText(String.valueOf(p.getMain().getTemp_max()) + " max");
                speed.setText(String.valueOf(p.getWind().getSpeed()) + " miles/hour");
                name.setText(String.valueOf(p.getName()));
                country.setText(String.valueOf(p.getSys().getCountry()));
                sunrise.setText(UnixTime(Long.parseLong(String.valueOf(p.getSys().getSunrise()))));
                sunset.setText(UnixTime(Long.parseLong(String.valueOf(p.getSys().getSunset()))));
                switch_icon(p.getWeather().get(i).getIcon());


            }


        }

    }
}
