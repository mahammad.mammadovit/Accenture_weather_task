package com.example.mahammadmammadov.weather_accenture.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mahammadmammadov.weather_accenture.Model.Database_Bean;
import com.example.mahammadmammadov.weather_accenture.Presenter.Constants;

import java.util.ArrayList;
import java.util.List;

public class Weather_database extends SQLiteOpenHelper {

    private static final String TAG = Weather_database.class.getSimpleName();
    private static final String TABLE_NAME = Weather_database.class.getName();

    public Weather_database(Context context) {
        super(context, Constants.Weather_datatable.DB_NAME, null, Constants.Weather_datatable.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Constants.Weather_datatable.CREATE_TABLE_QUERY);
        } catch (SQLException ex) {
            Log.d(TAG, ex.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.Weather_datatable.DROP_QUERY);
        this.onCreate(db);
        db.close();
    }

    public void delete_data() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.Weather_datatable.TABLE_NAME, null, null);
        db.close();
    }

    public void InsertData(String d, String d0, String d1, String d2, String d3, String d4,
                           String d5, String d6, String d7, String d8, String d9, String d10) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.Weather_datatable.MAIN, d);
        values.put(Constants.Weather_datatable.DESCRIPTION, d0);
        values.put(Constants.Weather_datatable.ICON, d1);
        values.put(Constants.Weather_datatable.TEMP, d2);
        values.put(Constants.Weather_datatable.HUMIDITY, d3);
        values.put(Constants.Weather_datatable.TEMP_MIN, d4);
        values.put(Constants.Weather_datatable.TEMP_MAX, d5);
        values.put(Constants.Weather_datatable.SPEED, d6);
        values.put(Constants.Weather_datatable.COUNTRY, d7);
        values.put(Constants.Weather_datatable.SUNRISE, d8);
        values.put(Constants.Weather_datatable.SUNSET, d9);
        values.put(Constants.Weather_datatable.NAME, d10);

        try {

            db.insert(Constants.Weather_datatable.TABLE_NAME, null, values);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        db.close();
    }

    public List<Database_Bean> getAllData() {
        List<Database_Bean> contactList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Constants.Weather_datatable.GET_PRODUCTS_QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Database_Bean contact = new Database_Bean();
                contact.setMain(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.MAIN)));
                contact.setDescription(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.DESCRIPTION)));
                contact.setIcon(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.ICON)));
                contact.setTemp(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.TEMP)));
                contact.setHumidity(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.HUMIDITY)));
                contact.setTemp_min(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.TEMP_MIN)));
                contact.setTemp_max(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.TEMP_MAX)));
                contact.setSpeed(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.SPEED)));
                contact.setCountry(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.COUNTRY)));
                contact.setSunrise(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.SUNRISE)));
                contact.setSunset(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.SUNSET)));
                contact.setName(cursor.getString(cursor.getColumnIndex(Constants.Weather_datatable.NAME)));

                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contactList;
    }

    public void updateValues(String ask, String d, String d0, String d1, String d2, String d3, String d4,
                             String d5, String d6, String d7, String d8, String d9, String d10) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.Weather_datatable.MAIN, d);
        values.put(Constants.Weather_datatable.DESCRIPTION, d0);
        values.put(Constants.Weather_datatable.ICON, d1);
        values.put(Constants.Weather_datatable.TEMP, d2);
        values.put(Constants.Weather_datatable.HUMIDITY, d3);
        values.put(Constants.Weather_datatable.TEMP_MIN, d4);
        values.put(Constants.Weather_datatable.TEMP_MAX, d5);
        values.put(Constants.Weather_datatable.SPEED, d6);
        values.put(Constants.Weather_datatable.COUNTRY, d7);
        values.put(Constants.Weather_datatable.SUNRISE, d8);
        values.put(Constants.Weather_datatable.SUNSET, d9);
        values.put(Constants.Weather_datatable.NAME, d10);

        db.update(Constants.Weather_datatable.TABLE_NAME, values, Constants.Weather_datatable.KEY_ID + " = ?",
                new String[]{String.valueOf(ask)});

        db.close();

    }

    public boolean TableNotEmpty() {
        SQLiteDatabase db = getWritableDatabase();

        Cursor mCursor = db.rawQuery("SELECT * FROM " + Constants.Weather_datatable.TABLE_NAME, null);
        Boolean rowExists;

        if (mCursor.moveToFirst()) {
            mCursor.close();
            rowExists = true;
            db.close();

        } else {
            // I AM EMPTY
            mCursor.close();
            rowExists = false;
            db.close();
        }
        db.close();
        return rowExists;
    }
}