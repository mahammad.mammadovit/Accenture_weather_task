package com.example.mahammadmammadov.weather_accenture.Presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.example.mahammadmammadov.weather_accenture.Model.Wheather_pojo;
import com.example.mahammadmammadov.weather_accenture.R;
import com.example.mahammadmammadov.weather_accenture.View.WeatherView;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyServices {

    WeatherView weatherView;
    private APIClient client;
    Context context;

    public MyServices(WeatherView view) {
        this.weatherView = view;
        if (this.client != null) {
            client = new APIClient();
        }

    }


    public void getException(int code, ProgressDialog dialog) {
        switch (code) {
            case 404:
                Toast.makeText(context, "not found", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                break;

            case 500:
                Toast.makeText(context, "server broken", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                break;

            case 200:
                //Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                break;

            default:
                Toast.makeText(context, "unknown error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                break;
        }

    }

    public void getWeather_data(double lat, double lon, String unt, String appid, String url) {

        final ProgressDialog dialog = new ProgressDialog((Context) weatherView);
        dialog.setMessage("Wait");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();

        final APIInterface apiInterface = APIClient.getClient(url, null).create(APIInterface.class);
        Call<Wheather_pojo> call = apiInterface.getDailyWeather(lat, lon, unt, appid);
        call.enqueue(new Callback<Wheather_pojo>() {

            @Override
            public void onResponse(Call<Wheather_pojo> call, Response<Wheather_pojo> response) {
                if (response.isSuccessful()) {
                    dialog.dismiss();
                    if (response != null) {
                        Wheather_pojo p = response.body();
                        weatherView.getWeather(p);
                    }

                } else {
                    getException(response.code(), dialog);
                }
            }

            @Override
            public void onFailure(Call<Wheather_pojo> call, Throwable t) {
                dialog.dismiss();
                if (t != null)
                    Toast.makeText(context, t.getCause().getMessage(), Toast.LENGTH_LONG).show();

                if (t.getClass().equals(TimeoutException.class)) {
                    Toast.makeText(context, String.valueOf(R.string.poor_con), Toast.LENGTH_SHORT).show();
                }
                if (t instanceof IOException) {
                    Toast.makeText(context, String.valueOf(R.string.poor_con), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


}