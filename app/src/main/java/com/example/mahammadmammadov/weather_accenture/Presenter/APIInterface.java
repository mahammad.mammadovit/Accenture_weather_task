package com.example.mahammadmammadov.weather_accenture.Presenter;

import com.example.mahammadmammadov.weather_accenture.Model.Wheather_pojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface APIInterface {

//    @GET("weather")
//    @Headers({"Content-Type: application/json;charset=UTF-8"})
//    Call<Wheather_pojo> getDailyWeather(@Query("q") String city_name, @Query("appid") String appid);

    @GET("weather")
    @Headers({"Content-Type: application/json;charset=UTF-8"})

    Call<Wheather_pojo> getDailyWeather(@Query("lat") double lat,
                                        @Query("lon") double lon,
                                        @Query("units") String units,
                                        @Query("appid") String appid);
}
